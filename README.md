# ADEX Operations Challenge

Included in this repository is a [Docker image][] for a service called
`summer`, as well as a manifest [deployment.yaml](./deployment.yaml) to
deploy it to a Kubernetes cluster.

The intended use of the service is to provide an HTTP endpoint `/sum`
with two numbers in query parameters `a` and `b`, and it will respond
with the sum of those two numbers:

```
$ curl 'http://localhost:8080/sum?a=123&b=456' ; echo
579
```

This service is used by 10-50 clients processing a total of 500-1000
requests per second.

## The Problems

However, this service has some problems:

- Once deployed, it often - but not always - restarts several times
  before running successfully. This sometimes causes issues when new
  versions are deployed or node failure occurs.

- When under moderate load, the service sometimes crashes. The
  development team has been unable to reproduce the issue locally and
  needs a more specific description of the conditions it fails under.

- Requests to the service fail infrequently, about 5% of the time.


We would like to know:

- Why does it take several attempts to start, and what could be done to
  address this?

- Why does it crash under load and what kind of load triggers it?
  Subject the service to load and try to reproduce the crash. What could
  be done to address this?

- What could be done to address the ambient failure rate?

Assume the service’s code cannot be changed easily; all problems need to
be solved at the operational level. Although the service has many
problems, it follows HTTP best practices - failure is always reported
with a correct status code and GET requests have no side effects.

Finally: The service exports several Prometheus metrics on the
`/metrics` endpoint. What other metrics would be of interest to monitor
the service, under usual conditions or to help diagnose issues like
these?


## Setting up a Kubernetes Cluster

If you don’t have a Kubernetes cluster available to test on, you can set
one up locally via [kind][] and the [cluster.yaml](./cluster.yaml) we
have provided. This will create a cluster named `adex-ops`.

```
$ kind create cluster --config ./cluster.yaml
$ docker pull registry.gitlab.com/theadex/ops-challenge/summer:v1
$ kind load docker-image registry.gitlab.com/theadex/ops-challenge/summer:v1 --name adex-ops
$ kubectl apply -f deployment.yaml
```

Once deployed, the service will be available on
`http://localhost:30001`.

You are free to use any Kubernetes deployment of your choice (e.g.
minikube, k3s), or even investigate directly with lower-level container
management tools.


[Docker image]: https://gitlab.com/theadex/ops-challenge/container_registry
[kind]: https://kind.sigs.k8s.io/
